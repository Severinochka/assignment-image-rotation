#include "../include/bmp.h"

struct image rotate(struct image to_rotate) {
    struct image rotated = {
        .width = to_rotate.height,
        .height = to_rotate.width
    };
    rotated.data = malloc(sizeof(struct pix) * to_rotate.width * to_rotate.height);

    for (size_t j = 0; j < to_rotate.height; j++){  
        for (size_t i = 0; i < to_rotate.width; i++){
            rotated.data[rotated.width-j-1+rotated.width*i] = to_rotate.data[to_rotate.width*j+i];
        }
    }
    return rotated;
}
