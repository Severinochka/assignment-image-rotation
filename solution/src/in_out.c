#include "../include/in_out.h"

enum open_status open_file(int argc, char **argv, char **in_p, char **out_p, FILE **in_file, FILE **out_file){
    if (argc != 3){ //check the number of args
        return INVALID_PARAMETERS_ARGUMENTS;
    }

    //paths to rotated pic
    *in_p = argv[1];
    *out_p = argv[2];

    *in_file = fopen(*in_p, "rb");
    if (*in_file == NULL){
        return FAILED_TO_OPEN_IN_FILE; //error - nothing in memory
    }

    *out_file = fopen(*out_p, "wb");
    if (*out_file == NULL){
        return FAILED_TO_OPEN_OUT_FILE; //same error ^
    }
    return 0;
}

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2) {
    fclose(*in_file);
    fclose(*out_file);
    free(pic1->data);
    free(pic2->data);
    return 0;
}