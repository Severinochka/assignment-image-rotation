#ifndef INOUT_H
#define INOUT_H

#include "./bmp.h"

enum open_status {
    OPEN_OK = 0,
    INVALID_PARAMETERS_ARGUMENTS,
    FAILED_TO_OPEN_IN_FILE,
    FAILED_TO_OPEN_OUT_FILE
};

enum close_status {
    CLOSE_OK = 0
};

enum open_status open_file(int argc, char **argv, char **in_p, char **out_p, FILE **in_file, FILE **out_file);

enum close_status close_file(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2);

#endif
