#include <../include/bmp.h>
#include <../include/in_out.h>
#include <../include/rotate.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    char *in_p = NULL;
    char *out_p = NULL;
    FILE *in_file = NULL;
    FILE *out_file = NULL;

    if (open_file(argc, argv, &in_p, &out_p, &in_file, &out_file) == INVALID_PARAMETERS_ARGUMENTS){
        fprintf(stderr, "Invalid parameters arguments\n");
        return 1;
    }
    if (open_file(argc, argv, &in_p, &out_p, &in_file, &out_file) == FAILED_TO_OPEN_IN_FILE) {
        fprintf(stderr, "Failed to open input file\n");
        return 1;
    }
    if (open_file(argc, argv, &in_p, &out_p, &in_file, &out_file) == FAILED_TO_OPEN_OUT_FILE) {
        fprintf(stderr, "Failed to open output file\n");
        return 1;
    }
    if (open_file(argc, argv, &in_p, &out_p, &in_file, &out_file) == OPEN_OK) {
        fprintf(stderr,"Initialization successful!\n");
    }

    struct image pic;

    enum read_status rs = from_bmp(in_file, &pic);
    if(rs != 0){
        //fprintf(stderr, "READ_ERROR: %u\n", rs);
        return READ_ERROR;
    }

    struct image rotated_pic = rotate(pic);

    enum write_status ws = to_bmp(out_file, &rotated_pic);
    if (ws != 0) { //check errors
        //fprintf(stderr, "WRITE_ERROR: %u\n", ws);
        return WRITE_ERROR;
    }
    close_file(&in_file, &out_file, &pic, &rotated_pic);

    return 0;
}
