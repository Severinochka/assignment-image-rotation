#include "../include/bmp.h"

enum read_status from_bmp( FILE* in, struct image* img ){
    if(!(in && img)){
        return READ_ERROR;
    }

    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);

    if (header.biSize != 40){
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24){
        return READ_INVALID_BITS;
    }
    if (header.bfType > 19778 || header.bfType <= 0) {
         return READ_INVALID_SIGNATURE;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(sizeof(struct pix)*img->width*img->height); //memory for pixs

    uint32_t padding = 4 - ((img->width * 3) % 4);

    for (int i = 0; i < img->height; i++) {
        fread(&img->data[i * img->width], 3, img->width, in);
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if(!(out && img)){
        return WRITE_ERROR;
    }

    uint32_t padding = 4 - ((img->width * 3) % 4);

    struct bmp_header header ={
        .bfType = 19778,
        .biSizeImage = (img->width*3+padding)*img->height,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .bfileSize = (img->width*3+padding)*img->height + sizeof(struct bmp_header)
    };
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    for (int i = 0; i < img->height; i++) {
        fwrite(&img->data[i * img->width], 3, img->width, out);
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}
